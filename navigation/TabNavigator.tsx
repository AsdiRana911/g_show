import React from 'react'
import {Text} from 'react-native'
import { Entypo } from '@expo/vector-icons';

import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../screens/TabScreens/HomeScreen'
import PostScreen from '../screens/TabScreens/PostScreen'
import SearchScreen from '../screens/TabScreens/SearchScreen'
import ProfileScreen from '../screens/TabScreens/ProfileScreen'
import NotificationScreen from '../screens/TabScreens/NotificationScreen'
import CameraScreen from '../screens/TabScreens/CameraScreen'

import { MaterialIcons, MaterialCommunityIcons, FontAwesome } from '@expo/vector-icons';

const Tab = createMaterialBottomTabNavigator();

export default function MyTabs() {
  return (
    <Tab.Navigator barStyle={{ backgroundColor: '#f5f5dc' }}>
      <Tab.Screen name="Home" component={HomeStack}
        
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen name="Search" component={SearchScreen}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({ color }) => (
            <MaterialIcons name="search" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen name="Post" component={PostStack}
        options={{
          tabBarLabel: 'Post',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="plus-box-outline" size={24} color={color} />
          ),
        }}
      />
      <Tab.Screen name="Notifications" component={NotificationScreen}
        options={{
          tabBarLabel: 'Notifications',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="heart" size={24} color={color} />
          ),
        }}
      />
      <Tab.Screen name="Profile" component={ProfileScreen}
        options={{
          tabBarLabel: 'Post',
          tabBarIcon: ({ color }) => (
            <FontAwesome name="user-circle" size={24} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

// In App.js in a new project


const Stack = createStackNavigator();

const PostStack = () => {
  return (
      <Stack.Navigator>
        <Stack.Screen name="Post" component={PostScreen}/>
        {/* <Stack.Screen name="Camera" component={CameraScreen} options = {{headerShown: false}} /> */}
      </Stack.Navigator>
  );
}

const HomeStack = () => {
  return (
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen}/>
        {/* <Stack.Screen name="Camera" component={CameraScreen} options = {{headerShown: false}} /> */}
      </Stack.Navigator>
  )
}