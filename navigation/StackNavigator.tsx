// In App.js in a new project

import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import TabNavigator from './TabNavigator'
import PostCheckout from '../screens/TabScreens/Upload/PostCheckout'
import SavedPostsScreen from '../screens/TabScreens/HomeHeaderScreens/SavedPostsScreen'
import ProfileScreen from '../screens/TabScreens/ProfileScreen'

const Stack = createStackNavigator();

function StackNavigator() {
  return (
      <Stack.Navigator>
        <Stack.Screen name="TabNavigator" component={TabNavigator} options = {{headerShown: false}} />
        <Stack.Screen name="PostCheckout" component={PostCheckout} />
        <Stack.Screen name="SavedPosts" component={SavedPostsScreen} />
        <Stack.Screen name="ProfileScreen" component={ProfileScreen} />
      </Stack.Navigator>
  );
}

export default StackNavigator;