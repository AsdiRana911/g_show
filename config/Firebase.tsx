import * as firebase from 'firebase';
import 'firebase/auth'
require ('firebase/firestore')
// Optionally import the services that you want to use
//import "firebase/auth";
//import "firebase/database";
//import "firebase/firestore";
//import "firebase/functions";
//import "firebase/storage";

// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyDjCmcxceIXRMRRTcP9Wgh4I1kl6O_qpiA",
    authDomain: "gshow-a094e.firebaseapp.com",
    projectId: "gshow-a094e",
    storageBucket: "gshow-a094e.appspot.com",
    messagingSenderId: "730370741556",
    appId: "1:730370741556:web:34a78b7d81903f18598161",
    measurementId: "G-VEG8PZV3GW"
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
 }else {
    firebase.app(); // if already initialized, use that one
 }

const db = firebase.firestore();

export default db;
