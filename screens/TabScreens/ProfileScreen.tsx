import React, { useEffect, useLayoutEffect } from 'react'
import { View, Text, TouchableOpacity, TextInput, Dimensions, Image } from 'react-native'
import { Feather } from '@expo/vector-icons';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getUser } from '../../actions/user'

const {height, width} = Dimensions.get('window')
const ProfileScreen = (props) => {
    const {params} = props.route

    useEffect(() => {
        if(params !== undefined){
            props.getUser(params, 'GET_PROFILE')

        }
    }, [])

    useLayoutEffect(() => {
        props.navigation.setOptions({
            title: props.profile.username,
            headerStyle: {
                elevation: 1
            }
        });
      }, [props.navigation]);

    if (params == undefined){
        return (
            <View style = {{flex: 1, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', flexGrow: 1}} >
                <Text>Your Profile</Text>
            </View>
        )
    } else{
        return (
            <View style = {{flex: 1, backgroundColor: 'white'}} >
                <View style = {{width: '100%', height: 120, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                    <Image source = {{uri: props.profile.photo}} style = {{height: 90, width: 90, borderRadius: 45, margin: 20}} />
                    <View style = {{flexDirection: 'row', justifyContent: 'space-around', width: '70%'}}>
                        <View style = {{justifyContent: 'center', alignItems: 'center'}}>
                            <Text style = {{fontSize: 20, fontWeight: 'bold'}}>23</Text>
                            <Text style = {{fontSize: 15}}>Posts</Text>
                        </View>
                        <View style = {{justifyContent: 'center', alignItems: 'center'}}>
                            <Text style = {{fontSize: 20, fontWeight: 'bold'}}>2343</Text>
                            <Text style = {{fontSize: 15}}>Followers</Text>
                        </View>
                        <View style = {{justifyContent: 'center', alignItems: 'center'}}>
                            <Text style = {{fontSize: 20, fontWeight: 'bold'}}>423</Text>
                            <Text style = {{fontSize: 15}}>Following</Text>
                        </View>
                    </View>
                </View>
                <View style = {{padding: 20, width: '100%'}}>
                    <Text>{props.profile.bio}</Text>
                </View>
                <View style = {{width: '100%', height: 60, flexDirection: 'row', justifyContent: 'center'}}>
                    {
                        props.profile.followers?.includes(props.user.uid) ?
                        <TouchableOpacity style = {{flexDirection: 'row', width: width*.45, height: 35, justifyContent: 'center', alignItems: 'center', borderWidth: 0.5, borderColor: 'grey', borderRadius: 7, margin: width*.0125}}>
                            <Text style = {{fontWeight: 'bold', fontSize: 16}}>Following</Text>
                            <Feather name="chevron-down" size={30} color="black" />
                        </TouchableOpacity>
                        :
                        <TouchableOpacity style = {{backgroundColor: '#0095f6', flexDirection: 'row', width: width*.45, height: 35, justifyContent: 'center', alignItems: 'center', borderWidth: 0.5, borderColor: 'grey', borderRadius: 7, margin: width*.0125}}>
                            <Text style = {{fontWeight: 'bold', fontSize: 16, color: 'white'}}>Follow</Text>
                        </TouchableOpacity>
                    }
                    <TouchableOpacity style = {{width: width*.45, height: 35, justifyContent: 'center', alignItems: 'center', borderWidth: 0.5, borderColor: 'grey', borderRadius: 7,  margin: width*.0125}}>
                        <Text style = {{fontWeight: 'bold', fontSize: 16}}>Message</Text>
                    </TouchableOpacity>
                </View> 
            </View>
        )
    }
    
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ getUser }, dispatch)
}

const mapStateToProps = (state) => {
    return{
        user: state.user,
        profile: state.profile
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(ProfileScreen)
