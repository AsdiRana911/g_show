import React, { useState, useEffect, useRef, useLayoutEffect } from 'react'
import { View, Text, TouchableOpacity, Platform, Dimensions, Image } from 'react-native'
import RBSheet from "react-native-raw-bottom-sheet";
import { Entypo, FontAwesome } from '@expo/vector-icons';
import { Camera } from 'expo-camera';
import * as ImagePicker from 'expo-image-picker';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getUser } from '../../actions/user'
import { uploadPhoto } from '../../actions/index'
import { updateNextPhoto, removeImage } from '../../actions/post'

const {height, width} = Dimensions.get('window')
const PostScreen = (props) => {
    const [imageChosen, setImageChosen] = useState(undefined)
    const [hasPermission, setHasPermission] = useState(false);
    const [type, setType] = useState(Camera.Constants.Type.back);
    useEffect(() => {
        async () => {
            if (Platform.OS !== 'web') {
              const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
              if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
              }
            }
            const { status } = await Camera.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        }
        console.log('Props=>>>>>>>>>>>>>>>>>>>',props)
     }, [])

     useLayoutEffect(() => {
        props.navigation.setOptions({
            title: 'New Post',
            headerLeft: () => (
              <Entypo name="cross" size={30} color="black" />
            ),
            headerRight: () => (
                <TouchableOpacity activeOpacity = {0.8} onPress = {() => uploadPost()}>
                    <Entypo name="arrow-long-right" size={30} color="blue" style = {{marginRight: 20}} />
                </TouchableOpacity>
            )
        });
      }, [props.navigation]);

      const uploadPost = () => {
          props.navigation.navigate('PostCheckout')
      }

     const pickImage = async() => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            // quality: 1,
          });
      
        console.log(result);
    
        if (!result.cancelled) {
            const url = await props.uploadPhoto(result);
            setImageChosen(url)
            props.updateNextPhoto(url)
        }
     }

    const removeImage = (url) => {
        const position = props.post.photos.indexOf(url)
        props.removeImage(position)
        if(props.post.photos.length === 2 || props.post.photos.length === 3){
            setImageChosen(props.post.photos[0])
        }else{
            setImageChosen(undefined)
        }
    }

    return (
        <View style = {{flex: 1, backgroundColor: 'white', flexGrow: 1, paddingTop: 50}} >
            {imageChosen == undefined || props.post.photos?.length === 0 ?
                (<TouchableOpacity style = {{height: 90, width: 90, borderRadius: 45, backgroundColor: 'rgba(0,0,0,0.15)', alignItems: 'center', justifyContent: 'center', alignSelf: 'center'}} onPress = {pickImage} activeOpacity = {0.7}>
                        <Text style = {{color: 'white', fontSize: 40}}>+</Text>
                </TouchableOpacity>)
            : 
                <View style = {{height: 350, width, }}>
                    <Image source = {{uri: imageChosen}} style = {{width,height: 350, resizeMode: 'cover', marginBottom: 10}} />
                    <TouchableOpacity style ={{position: 'absolute', bottom: 30, right: 30}} onPress = {() => removeImage(imageChosen)}>
                        <FontAwesome name = 'trash' color = 'black' size = {25} />
                    </TouchableOpacity>
                </View>
            }
            <View style = {{width, alignItems: 'center', marginTop: 10, flexDirection: 'row', justifyContent: 'center'}}>
                
                {
                    (props.post.photos == undefined || props.post.photos?.length === 3 || props.post.photos?.length === 0) ? null :
                    <TouchableOpacity style = {{height: 90, width: 90, backgroundColor: 'rgba(0,0,0,0.1)', alignItems: 'center', justifyContent: 'center', borderRadius: 12, margin: 5}} onPress = {pickImage} activeOpacity = {0.7}>
                        <View style = {{height: 50, width: 50, borderRadius: 25, backgroundColor: 'rgba(0,0,0,0.1)', alignItems: 'center', justifyContent: 'center'}}>
                            <Text style = {{color: 'white', fontSize: 28}}>+</Text>
                        </View>
                    </TouchableOpacity>
                }
                {
                    props.post.photos?.map(e=>
                        <TouchableOpacity key = {e} style = {{borderRadius: 12, margin: 5}} onPress = {() => setImageChosen(e)} activeOpacity = {0.8}>
                            <Image source={{uri: e}} style = {{width: 90, height: 90, borderRadius: 12}} />
                        </TouchableOpacity>
                    )
                }
            </View>
            {/* <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={false}
                customStyles={{
                wrapper: {
                    backgroundColor: "transparent"
                },
                draggableIcon: {
                    backgroundColor: "#000"
                }
                }}
            >
                <View style = {{backgroundColor: 'rgba(0,0,0,0.2)', flex: 1, padding: 20, paddingTop: 30}}>
                    <TouchableOpacity style = {{flexDirection: 'row', alignItems: 'center', marginBottom: 20}} activeOpacity = {0.5}>
                        <Entypo name="camera" size={34} color="black" />
                        <Text style = {{marginLeft: 20, fontSize: 18}}>Open Camera</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style = {{flexDirection: 'row', alignItems: 'center'}} activeOpacity = {0.5} onPress = {() => pickImage()}>
                        <Entypo name="image-inverted" size={34} color="black" />
                        <Text style = {{marginLeft: 20, fontSize: 18}}>Open gallery</Text>
                    </TouchableOpacity>
                </View>
            </RBSheet> */}
        </View>
    )
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ getUser, uploadPhoto, updateNextPhoto, removeImage }, dispatch)
}

const mapStateToProps = (state) => {
    return{
        user: state.user,
        post: state.post
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(PostScreen)
