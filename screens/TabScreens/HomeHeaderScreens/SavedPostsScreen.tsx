import React, { useEffect, useLayoutEffect } from 'react'
import { View, Text, TouchableOpacity, TextInput, Dimensions, FlatList, Image, SafeAreaView } from 'react-native'
import { Entypo, Ionicons } from '@expo/vector-icons';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getUser } from '../../../actions/user'
import { getSavedPosts, likePost, unlikePost, savePost, unSavePost } from '../../../actions/post'

import PostComponent from '../../Components/PostComponent'

const {height, width} = Dimensions.get('window')
const SavedPostsScreen = (props) => {

    useEffect(() => {
        props.getSavedPosts()
    }, [])
    
    useLayoutEffect(() => {
        props.navigation.setOptions({
            title: 'Saved Posts',
            headerRight: () => (
                    <TouchableOpacity style = {{marginRight: 10}} activeOpacity = {0.8}>
                        <Image source = {require('../../../assets/messenger.png')} style = {{height: 35, width: 35}} />
                    </TouchableOpacity>
            )
        });
      }, [props.navigation]);

    return (
        <SafeAreaView style = {{flex: 1, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', flexGrow: 1}} >
            <FlatList 
                data = {props.post.saved_feed}
                keyExtractor = {(item) => JSON.stringify(item.id)}
                renderItem = {({item}) => (
                    <PostComponent
                        item = {item}
                        user = {props.user}
                        likePost = {(item) => props.likePost(item)}
                        unlikePost = {(item) => props.unlikePost(item)}
                        savePost = {(item) => props.savePost(item)}
                        unSavePost = {(item) => props.unSavePost(item)}
                    />
                )}
            />
        </SafeAreaView>
    )
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ getUser, getSavedPosts, likePost, unlikePost, savePost, unSavePost }, dispatch)
}

const mapStateToProps = (state) => {
    return{
        user: state.user,
        post: state.post
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(SavedPostsScreen)
