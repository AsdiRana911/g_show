import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, Dimensions } from 'react-native'


import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { updateEmail, updatePassword, login } from '../../actions/user'

const {height, width} = Dimensions.get('window')
class NotificationScreen extends Component {

    render() {
        return (
            <View style = {{flex: 1, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', flexGrow: 1}} >
                <Text>NotificationScreen</Text>
            </View>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ updateEmail, updatePassword, login }, dispatch)
}

const mapStateToProps = (state) => {
    return{
        user: state.user,
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(NotificationScreen)
