import React, { useEffect, useLayoutEffect } from 'react'
import { View, Text, TouchableOpacity, TextInput, Dimensions, FlatList, Image, SafeAreaView } from 'react-native'
import { Entypo, Ionicons } from '@expo/vector-icons';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getUser } from '../../actions/user'
import { getPosts, likePost, unlikePost, savePost, unSavePost } from '../../actions/post'

import PostComponent from '../Components/PostComponent'

const {height, width} = Dimensions.get('window')
const HomeScreen = (props) => {

    useEffect(() => {
        props.getPosts()
    }, [])
    
    useLayoutEffect(() => {
        props.navigation.setOptions({
            title: 'G_SHOW',
            headerRight: () => (
                <View style = {{flexDirection: 'row', alignItems: 'center'}}>
                    <TouchableOpacity style = {{marginRight: 10}} activeOpacity = {0.8} onPress = {() => props.navigation.navigate('SavedPosts')}>
                        <Ionicons name="heart" size={30} color="red" style = {{margin: 10}} />
                    </TouchableOpacity>
                    <TouchableOpacity style = {{marginRight: 10}} activeOpacity = {0.8}>
                        <Image source = {require('../../assets/messenger.png')} style = {{height: 35, width: 35}} />
                    </TouchableOpacity>
                </View>
            )
        });
      }, [props.navigation]);

    return (
        <SafeAreaView style = {{flex: 1, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', flexGrow: 1}} >
                <FlatList 
                    data = {props.post.feed}
                    keyExtractor = {(item, index) => index.toString()}
                    renderItem = {({item}) => (
                        <PostComponent
                            item = {item}
                            user = {props.user}
                            likePost = {(item) => props.likePost(item)}
                            unlikePost = {(item) => props.unlikePost(item)}
                            savePost = {(item) => props.savePost(item)}
                            unSavePost = {(item) => props.unSavePost(item)}
                            navigation = {props.navigation}
                        />
                    )}
                />
        </SafeAreaView>
    )
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ getUser, getPosts, likePost, unlikePost, savePost, unSavePost }, dispatch)
}

const mapStateToProps = (state) => {
    return{
        user: state.user,
        post: state.post
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(HomeScreen)
