import React, { useLayoutEffect } from 'react'
import { View, Text, TouchableOpacity, TextInput, Dimensions, ScrollView, Image } from 'react-native'
import { Feather, FontAwesome } from '@expo/vector-icons';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { updateEmail, updatePassword, login } from '../../../actions/user'
import { updateCaption, uploadPost, removeAllImages } from '../../../actions/post'

const {height, width} = Dimensions.get('window')
const PostCheckout = (props) => {

    const uploadPost = () => {
        props.navigation.navigate('TabNavigator')
        alert('Posted')
        props.uploadPost()
        props.removeAllImages()
        // props.getPosts()
    }
    useLayoutEffect(() => {
        props.navigation.setOptions({
            title: 'Add a caption',
            headerLeft: () => (
                <Feather name="arrow-left" size={30} color="black" />
            ),
            headerRight: () => (
                <TouchableOpacity activeOpacity = {0.8} onPress = {() => uploadPost()}>
                    <FontAwesome name="paper-plane" size={25} color="black" style = {{marginRight: 20}} />
                </TouchableOpacity>
            )
        });
      }, [props.navigation]);

    return (
        <View style = {{flex: 1, alignItems: 'center', paddingTop: 30, flexGrow: 1}} >
            <TextInput
                style = {{backgroundColor: 'white', margin: 10, paddingHorizontal: 15, paddingVertical: 5, fontSize: 20, borderRadius: 10, width: '95%'}}
                placeholderTextColor = {'grey'}
                placeholder = {'Type in caption'}
                value = {props.description}
                onChangeText = {input => props.updateCaption(input)}
                multiline = {true}
            />
            <View>
                <ScrollView horizontal pagingEnabled>
                {
                    props.post.photos?.map(e=>
                        <Image source={{uri: e}} style = {{width, height: 350}} />
                    )
                }
                </ScrollView>
            </View>
        </View>
    )
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ updateCaption, uploadPost, removeAllImages }, dispatch)
}

const mapStateToProps = (state) => {
    return{
        user: state.user,
        post: state.post
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(PostCheckout)
