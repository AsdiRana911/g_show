import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, Dimensions } from 'react-native'
import * as firebase from 'firebase';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getUser } from '../../actions/user'

const {height, width} = Dimensions.get('window')
class Welcome extends Component {

    componentDidMount = () => {
        firebase.auth().onAuthStateChanged((user) => {
            console.log(user)
            if(user){
                this.props.getUser(user.uid);
                if(this.props.user != null){
                    this.props.navigation.navigate('StackNavigator');
                    this.props.navigation.reset({
                        index: 0,
                        routes: [{name: 'StackNavigator'}]
                    })
                }
            }else{
                this.props.navigation.navigate('Login')
            }
        })
    }

    render() {
        return (
            <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text>Waiting</Text>
            </View>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ getUser }, dispatch)
}

const mapStateToProps = (state) => {
    return{
        user: state.user,
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(Welcome)
