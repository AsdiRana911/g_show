import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, Dimensions } from 'react-native'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { Feather } from '@expo/vector-icons'; 

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { updateEmail, updatePassword, login } from '../../actions/user'

const {height, width} = Dimensions.get('window')
class Login extends Component {

    render() {
        return (
            <KeyboardAwareScrollView style = {{flex: 1, backgroundColor: 'white'}}  contentContainerStyle = {{alignItems: 'center', justifyContent: 'center', flexGrow: 1}} >
                <TextInput
                    style = {{height: 50, width: width*0.9, backgroundColor: 'white', paddingHorizontal: 20, marginBottom: 20, borderRadius: 5, borderColor: 'grey', borderWidth: 1}}
                    placeholderTextColor = {'grey'}
                    placeholder = {'Email or username'}
                    value = {this.props.user.email}
                    onChangeText = {input => this.props.updateEmail(input)}
                />
                <View style = {{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, alignItems: 'center', width: width*0.9, height: 50, backgroundColor: 'white', marginBottom: 20, borderRadius: 5, borderColor: 'grey', borderWidth: 1}}>
                    <TextInput
                        style = {{width: '92%'}}
                        placeholderTextColor = {'grey'}
                        placeholder = {'Password'}
                        secureTextEntry = {true}
                        value = {this.props.user.password}
                        onChangeText = {input => this.props.updatePassword(input)}
                    />
                    <Feather name="eye-off" size={24} color="grey" />
                </View>
                <TouchableOpacity style = {{alignItems: 'center', justifyContent: 'center', backgroundColor: '#11aaee', width: width*0.9, height: 50, marginBottom: 10, borderRadius: 5}} activeOpacity = {0.7}
                    onPress = {() => this.props.login()}
                >
                    <Text style = {{color: 'white', fontSize: 18}}>Log In</Text>
                </TouchableOpacity>

                <View style = {{flexDirection: 'row', width: width *0.9, alignItems: 'center', justifyContent: 'center', marginBottom: 20}}>
                    <Text style = {{color: 'grey', fontSize: 12}}>Forgot your login details? </Text>
                    <TouchableOpacity activeOpacity = {1}>
                        <Text style = {{fontSize: 12, color: '#000088', fontWeight: 'bold'}}>Get help logging in</Text>
                    </TouchableOpacity>
                </View>
                <View style = {{position: 'absolute', height: 70, width, bottom: 0, borderTopWidth: 1, borderColor: 'grey', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                    <Text style = {{color: 'grey', fontSize: 12}}>Don't have an account? </Text>
                    <TouchableOpacity onPress = {() => this.props.navigation.navigate('Signup')} activeOpacity = {1}>
                        <Text style = {{fontSize: 12, color: '#000088', fontWeight: 'bold'}}>Sign up.</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ updateEmail, updatePassword, login }, dispatch)
}

const mapStateToProps = (state) => {
    return{
        user: state.user,
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(Login)
