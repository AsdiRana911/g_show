import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, Dimensions, Image } from 'react-native'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker';
import { Feather } from '@expo/vector-icons';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { uploadPhoto } from '../../actions/index'
import { updateEmail, updatePassword, updateUserName, signup, updatePhoto } from '../../actions/user'

const {height, width} = Dimensions.get('window')

class Signup extends Component {
    state = {
        image: '',
        confirmPassword: '',
        showPassword: false,
        showConfirmPassword: false,
    }

    pickImage = async () => {
        try{
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
            if (status == 'granted'){
                let result = await ImagePicker.launchImageLibraryAsync({
                    allowsEditing: true,
                    aspect: [4, 3],
                    quality: 1,
                  });
              
                  console.log(result);
              
                  if (!result.cancelled) {
                      const url = await this.props.uploadPhoto(result)
                      this.props.updatePhoto(url)
                      console.log('TTTTTTTTTT', this.props.user.photo)
                    // this.setState({image: result.uri});
                  }
            }
        }catch(e){
            alert(e)
        }
        
      };

    onSignupPress = () => {
        // alert(this.props.user.userName)
        if(this.props.user.password === this.state.confirmPassword && this.props.user.userName !== ''){
            this.props.signup();
        } else{
            alert('the passwords are different')
        }        
    }

    render() {
        return (
            <KeyboardAwareScrollView style = {{flex: 1, backgroundColor: 'white'}}  contentContainerStyle = {{alignItems: 'center', justifyContent: 'space-between', flexGrow: 1}} >
                <View style = {{alignItems: 'center', marginTop: 30}}>
                    <TouchableOpacity activeOpacity = {0.9} style = {{height: width*0.35, width: width*0.35, borderRadius: 1000, borderWidth: 3, marginBottom: 20, alignItems: 'center', justifyContent: 'center'}}
                        onPress = {
                            () => {
                                this.pickImage()
                            }
                        }
                    >
                        {this.props.user.photo == undefined ? 
                            <Feather name="user" size={100} color="black" />
                            :
                            <Image style = {{height: width*0.35, width: width*0.35, borderRadius: 1000}} source = {{uri: this.props.user.photo}} />}
                    </TouchableOpacity>
                    <TextInput
                        style = {{height: 50, width: width*0.9, backgroundColor: 'white', paddingHorizontal: 20, marginBottom: 20, borderRadius: 5, borderColor: 'grey', borderWidth: 1}}
                        placeholderTextColor = {'grey'}
                        placeholder = {'Email or username'}
                        value = {this.props.user.email}
                        onChangeText = {input => this.props.updateEmail(input)}
                    />
                    <TextInput
                        style = {{height: 50, width: width*0.9, backgroundColor: 'white', paddingHorizontal: 20, marginBottom: 20, borderRadius: 5, borderColor: 'grey', borderWidth: 1}}
                        placeholderTextColor = {'grey'}
                        placeholder = {'Username'}
                        value = {this.props.user.userName}
                        onChangeText = {input => this.props.updateUserName(input)}
                    />
                    <View style = {{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, alignItems: 'center', width: width*0.9, height: 50, backgroundColor: 'white', marginBottom: 20, borderRadius: 5, borderColor: 'grey', borderWidth: 1}}>
                        <TextInput
                            style = {{width: '92%'}}
                            placeholderTextColor = {'grey'}
                            placeholder = {'Password'}
                            secureTextEntry = {!this.state.showPassword}
                            value = {this.props.user.password}
                            onChangeText = {input => this.props.updatePassword(input)}
                        />
                        <TouchableOpacity activeOpacity = {0.9} onPress = {() => this.setState({showPassword: !this.state.showPassword})}>
                            {this.state.showPassword ? <Feather name = "eye" size = {24} color = "grey" /> : <Feather name="eye-off" size={24} color="grey" />}
                        </TouchableOpacity>
                    </View>
                    <View style = {{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, alignItems: 'center', width: width*0.9, height: 50, backgroundColor: 'white', marginBottom: 20, borderRadius: 5, borderColor: 'grey', borderWidth: 1}}>
                        <TextInput
                            style = {{width: '92%'}}
                            placeholderTextColor = {'grey'}
                            placeholder = {'Confirm Password'}
                            secureTextEntry = {!this.state.showConfirmPassword}
                            value = {this.state.confirmPassword}
                            onChangeText = {input => this.setState({confirmPassword: input})}
                        />
                        <TouchableOpacity activeOpacity = {0.9} onPress = {() => this.setState({showConfirmPassword: !this.state.showConfirmPassword})}>
                            {this.state.showConfirmPassword ? <Feather name = "eye" size = {24} color = "grey" /> : <Feather name="eye-off" size={24} color="grey" />}
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style = {{alignItems: 'center', justifyContent: 'center', backgroundColor: '#11aaee', width: width*0.9, height: 50, marginBottom: 10, borderRadius: 5}} activeOpacity = {0.7}
                        onPress = {this.onSignupPress}
                    >
                        <Text style = {{color: 'white', fontSize: 18}}>Sign Up</Text>
                    </TouchableOpacity>
                </View>
                <View style = {{height: 70, width, borderTopWidth: 1, borderColor: 'grey', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                    <Text style = {{color: 'grey', fontSize: 12}}>Already have an account? </Text>
                    <TouchableOpacity onPress = {() => this.props.navigation.navigate('Login')} activeOpacity = {1}>
                        <Text style = {{fontSize: 12, color: '#000088', fontWeight: 'bold'}}>Log in.</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ updateEmail, updatePassword, updateUserName, signup, uploadPhoto, updatePhoto }, dispatch)
}

const mapStateToProps = (state) => {
    return{
        user: state.user,
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(Signup)