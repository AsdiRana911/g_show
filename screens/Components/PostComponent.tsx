import React, { useState } from 'react'
import { View, Text, Image, Dimensions, ScrollView, TouchableOpacity, TouchableWithoutFeedback } from 'react-native'
import { Ionicons, EvilIcons } from '@expo/vector-icons';
import moment from 'moment'
const PostComponent = (props) => {
    const {height, width} = Dimensions.get('window')
    const [liked, setLiked] = useState(undefined)
    const [numLike, setNumLike] = useState(0)
    const [saved, setSaved] = useState(undefined)

    let lastTap = null;
    const handleDoubleTap = () => {
      const now = Date.now();

      const DOUBLE_PRESS_DELAY = 300;
      if (lastTap && (now - lastTap) < DOUBLE_PRESS_DELAY) {
        likePost();
      } else {
        lastTap = now;
      }
    }

    const likePost = () => {
        if((props.item.likes.includes(props.user.uid)) || liked === true){
            if(liked === false){
                setLiked(true)
                props.likePost(props.item)
                setNumLike(numLike + 1)
            }else{
                setLiked(false)
                props.unlikePost(props.item)
                setNumLike(numLike - 1)
            }
        } else{
            setLiked(true)
            props.likePost(props.item)
            setNumLike(numLike + 1)
        }
    }

    const savePost = () => {
        if((props.item.savedBy.includes(props.user.uid)) || saved === true){
            if(liked === false){
                setSaved(true)
                props.savePost(props.item)
            }else{
                setSaved(false)
                props.unSavePost(props.item)
            }
        } else{
            setSaved(true)
            props.savePost(props.item)
        }
    }

    return (
        <View>
            <View style = {{width, height: 70, backgroundColor: 'white', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderBottomColor: 'grey', borderBottomWidth: 0.14}}>
                <TouchableOpacity style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginLeft: 10}} activeOpacity = {0.7} onPress = {() => props.navigation.navigate('ProfileScreen', props.item.uid)}>
                    <Image source = {{uri: props.item.photo}} style = {{width: 40, height: 40, borderRadius: 20, marginRight: 15}} />
                    <Text style = {{fontWeight: 'bold', fontSize: 18}}>{props.item.username}</Text>
                </TouchableOpacity>
            </View>
            <View>
                <ScrollView horizontal pagingEnabled showsHorizontalScrollIndicator = {false}>
                    {
                        props.item.photos?.map(e =>
                            <TouchableWithoutFeedback onPress = {() => handleDoubleTap()}>
                                <Image key = {e.toString()} source = {{uri: e}} style = {{width, height: 360}} />
                            </TouchableWithoutFeedback>
                        )
                    }
                </ScrollView>
            </View>
            <View style = {{width, flexDirection: 'row', justifyContent: 'space-between', height: 50, alignItems: 'center'}}>
                <View style = {{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                    <TouchableOpacity onPress = {() => likePost()}>
                        {
                            (props.item.likes.includes(props.user.uid) && liked === undefined)
                            ?
                                <Ionicons name="heart" size={30} color="red" style = {{margin: 10}} />
                            :
                                (liked === true)
                                ?
                                    <Ionicons name="heart" size={30} color="red" style = {{margin: 10}} />
                                :
                                    <Ionicons name="heart-outline" size={30} color="black" style = {{margin: 10}} />
                        }
                    </TouchableOpacity>
                    <EvilIcons name="comment" size={35} color="black" style = {{marginRight: 10}} />
                    <Ionicons name="paper-plane-outline" size={25} color="black" />
                </View>
                <TouchableOpacity onPress = {() => savePost()}>
                    {
                        (props.item.savedBy.includes(props.user.uid) && saved == undefined) ?
                            <Image source = {require('../../assets/saved.png')} />
                        :
                            (saved === true) ?
                                <Image source = {require('../../assets/saved.png')} />
                            :
                                <Image source = {require('../../assets/save.png')} />
                    }
                </TouchableOpacity>
            </View>
            <Text style = {{fontWeight: 'bold', marginLeft: 10, marginBottom: 5}}>{
                props.item.likes.length + numLike
            } likes</Text>
            <View style = {{flexDirection: 'row'}}>
                <Text style = {{fontWeight: 'bold', marginLeft: 10}}>{props.item.username.toLowerCase()} </Text>
                <Text>{props.item.caption}</Text>
            </View>
            <TouchableOpacity activeOpacity = {0.7}>
                <Text style = {{marginLeft: 10, marginVertical: 2, color: 'grey'}}>View all {props.item.comments.length} comments</Text>
            </TouchableOpacity>
            <View style = {{flexDirection: 'row', alignItems: 'center', marginLeft: 10, marginTop: 5}}>
                <Image source = {{uri: props.user.photo}} style = {{height: 30, width: 30, borderRadius: 15}} />
                <Text style = {{marginLeft: 10, color: 'grey'}}>Add a comment</Text>
            </View>
            <Text style = {{marginLeft: 15, marginTop: 5, color: 'grey', fontSize: 10}}>{moment(props.item.date).format('ll')}</Text>
        </View>
    )
}

export default PostComponent
