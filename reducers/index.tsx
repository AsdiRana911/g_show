import { combineReducers} from 'redux'

const replaceAll = (str, find, replace) => {
    var escapedFind=find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    return str.replace(new RegExp(escapedFind, 'g'), replace);
}

const profile = (state ={}, action) => {
    switch(action.type){
        case 'GET_PROFILE':
            return action.payload
        default:
            return state
    }
}

const user = (state = {}, action) => {
    switch(action.type){
        case 'LOGIN':
            return action.payload
        case 'UPDATE_EMAIL':
            return{...state, email: action.payload}
        case 'UPDATE_PASSWORD':
            return{...state, password: action.payload}
        case 'UPDATE_USERNAME':
            return{...state, username: replaceAll(action.payload.toLowerCase(), ' ', '_')}
        case 'UPDATE_PHOTO':
            return{...state, photo: action.payload}
        default:
            return state;
    }
}

const post = (state = {}, action) => {
    switch(action.type){
        case 'UPDATE_POST_NEXT_PHOTO':
            return {...state, photos: action.payload}
        case 'DELETE_POST_PHOTO':
            return {...state, photos: action.payload}
        case 'DELETE_All_POST_PHOTO':
            return {...state, photos: action.payload}
        case 'UPDATE_EMAIL':
            return {...state, caption: action.payload}
        case 'GET_POSTS':
            return {...state, feed: action.payload}
            case 'GET_SAVED_POSTS':
            return {...state, saved_feed: action.payload}
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    user,
    post,
    profile
})

export default rootReducer;